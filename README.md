# PGP-Discord-Integration

A plugin that involves injecting a framework into discord to then embed PGP functionality in a seamless manner between users who share a private key.

Instructions to install Enhanced_Discord are located here: [Enhanced Discord](https://github.com/joe27g/EnhancedDiscord)

Overwrite files in this repo onto the vanilla installation and enable in the plugin panel under settings.

The private key for now is defined in the top lines of [discord_encryption.js](https://gitlab.com/unwound/pgp-discord-integration/blob/master/plugins/discord_encryption.js)